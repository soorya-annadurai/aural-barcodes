import numpy
import scipy
import soundfile
# import time
#
# import simpleaudio

from scipy.fft import *
from scipy.io import wavfile
import matplotlib.pyplot as plt
from reedsolomon.reedsolo import RSCodec, ReedSolomonError


class AuralBarcodeListener:
    barcode_filename: str = ''
    num_seconds_per_audio_chunk: float = 0
    frequency_steps: dict = {}
    inverted_frequency_steps: dict = {}
    barcode_zone_timing: str = ''
    message: str = ''

    def __init__(self, barcode_filename: str, num_seconds_per_audio_chunk: float, frequency_steps: dict,
                 barcode_zone_timing: str):
        if num_seconds_per_audio_chunk <= 0:
            raise ValueError('Must have more than 0 seconds per chunk of audio parsed')
        else:
            self.num_seconds_per_chunk = num_seconds_per_audio_chunk

        if len(frequency_steps) != 16:
            raise ValueError('This program assumes valid conversion from a hex to a frequency')
        else:
            self.frequency_steps = frequency_steps
            self.inverted_frequency_steps = {v: k for k, v in frequency_steps.items()}

        if not str(barcode_filename).endswith('.wav'):
            raise ValueError('Invalid filename extension')
        else:
            self.barcode_filename = barcode_filename

        self.barcode_zone_timing = barcode_zone_timing

    @staticmethod
    def __find_peak(magnitude_values, noise_level=2000):
        splitter = 0
        # zero out low values in the magnitude array to remove noise (if any)
        magnitude_values = numpy.asarray(magnitude_values)
        low_values_indices = magnitude_values < noise_level  # Where values are low
        magnitude_values[low_values_indices] = 0  # All low values will be zero out

        indices = []

        flag_start_looking = False

        both_ends_indices = []

        length = len(magnitude_values)
        for i in range(length):
            if magnitude_values[i] != splitter:
                if not flag_start_looking:
                    flag_start_looking = True
                    both_ends_indices = [0, 0]
                    both_ends_indices[0] = i
            else:
                if flag_start_looking:
                    flag_start_looking = False
                    both_ends_indices[1] = i
                    # add both_ends_indices in to indices
                    indices.append(both_ends_indices)

        return indices

    @staticmethod
    def __extract_frequency(indices, freq_bins: numpy.ndarray, freq_threshold=2):
        extracted_freqs = []

        for index in indices:
            freqs_range = freq_bins[index[0]: index[1]]
            avg_freq = round(numpy.average(freqs_range))

            if avg_freq not in extracted_freqs:
                extracted_freqs.append(avg_freq)

        # group extracted frequency by nearby=freq_threshold (tolerate gaps=freq_threshold)
        group_similar_values = numpy.split(numpy.array(extracted_freqs),
                                           numpy.where(numpy.diff(extracted_freqs) > freq_threshold)[0] + 1)

        # calculate the average of similar value
        extracted_freqs = []
        for group in group_similar_values:
            extracted_freqs.append(round(numpy.average(group)))

        print("freq_components", extracted_freqs)
        return extracted_freqs

    def parse_v1(self):
        audio_samples, sample_rate = soundfile.read(self.barcode_filename, dtype='int16')
        number_samples = len(audio_samples)

        # # duration of the audio file in seconds
        # duration = round(number_samples / sample_rate, 2)
        # print('Audio Duration: {0}s'.format(duration))

        # split the complete audio sample into one second each
        number_samples_in_chunk = int(sample_rate * self.num_seconds_per_chunk)
        for i in range(0, len(audio_samples), number_samples_in_chunk):
            chunk_start_index = i
            chunk_end_index = i + number_samples_in_chunk
            if chunk_end_index >= len(audio_samples):
                chunk_end_index = len(audio_samples) - 1
            if chunk_start_index >= chunk_end_index:
                break
            audio_chunk = audio_samples[chunk_start_index:chunk_end_index]

            # list of possible frequencies bins
            freq_bins = numpy.arange(number_samples_in_chunk) * sample_rate / number_samples_in_chunk
            # print('Frequency Length: ', len(freq_bins))
            # print('Frequency bins: ', freq_bins)

            # # TODO: Delete. I don't actually want to play.
            # play_obj = simpleaudio.play_buffer(audio_chunk, 1, 2, sample_rate)
            # play_obj.wait_done()
            # time.sleep(1)

            #     # FFT calculation
            fft_data = scipy.fft.fft(audio_chunk)
            # print('FFT Length: ', len(fft_data))
            # print('FFT data: ', fft_data)

            freq_bins = freq_bins[range(number_samples_in_chunk // 2)]
            normalization_data = fft_data / number_samples_in_chunk
            magnitude_values = normalization_data[range(len(fft_data) // 2)]
            magnitude_values = numpy.abs(magnitude_values)

            indices = self.__find_peak(magnitude_values=magnitude_values, noise_level=200)
            frequencies = self.__extract_frequency(indices=indices, freq_bins=freq_bins)
            print("frequencies:", frequencies)

    def parse(self):
        # Source: https://stackoverflow.com/a/66892766
        # Open the file and convert to mono
        sample_rate, data = wavfile.read(self.barcode_filename)
        if data.ndim > 1:
            data = data[:, 0]
        else:
            pass

        num_samples = len(data)
        number_samples_in_chunk = int(sample_rate * self.num_seconds_per_chunk)
        hex_message: str = ''
        for i in range(0, num_samples, number_samples_in_chunk):
            chunk_start_index = i
            chunk_end_index = i + number_samples_in_chunk
            if chunk_end_index >= num_samples:
                chunk_end_index = num_samples - 1
            if chunk_start_index >= chunk_end_index:
                break
            audio_chunk = data[chunk_start_index:chunk_end_index]

            # Fourier Transform
            N = len(audio_chunk)
            yf = rfft(audio_chunk)
            xf = rfftfreq(N, 1 / sample_rate)

            # Uncomment these to see the frequency spectrum as a plot
            # plt.plot(xf, numpy.abs(yf))
            # plt.show()

            # Get the most dominant frequency and return it
            idx = numpy.argmax(numpy.abs(yf))
            freq = xf[idx]
            # print("Dominant frequency: " + str(freq))

            def myround(x, base=200): # TODO: Accept this base as a parameter from main. And it should somehow know that it's a multiple of the first step offset.
                return base * round(x / base)

            # print("Rounded frequency: " + str(myround(freq)))
            hex_value = self.inverted_frequency_steps[myround(freq)]
            # print("Hex value: " + str(hex_value))
            hex_message += str(hex(hex_value)[2:])
            # print("Updated hex message: " + hex_message)
        binary_message_with_ecc_and_timing = bytearray.fromhex(hex_message)
        if not binary_message_with_ecc_and_timing.startswith(self.barcode_zone_timing.encode('utf-8')):
            raise RuntimeError("Unexpected timing zone at start")
        if not binary_message_with_ecc_and_timing.endswith(self.barcode_zone_timing.encode('utf-8')):
            raise RuntimeError("Unexpected timing zone at end")

        binary_message_with_ecc = binary_message_with_ecc_and_timing[len(self.barcode_zone_timing):-len(self.barcode_zone_timing)]
        self.message = RSCodec().decode(binary_message_with_ecc)[0].decode('utf-8')

        return self

    def decode_message(self):
        print("Decoded message: " + str(self.message))
        return self
