from .tone_builder import ToneBuilder

import binascii
import wave
from reedsolomon.reedsolo import RSCodec, ReedSolomonError


class AuralBarcodeBuilder:
    sampling_frequency: int = 0  # sampling frequency (in Hertz: the number of samples per second)
    channel_count: int = 0  # the number of channels (1=mono, 2=stereo)
    barcode_data: bytearray = bytearray(b'')
    num_seconds_per_audio_chunk: float = 0
    frequency_steps: dict = {}
    barcode_filename: str = ''
    barcode_zone_timing: str = ''
    message_utf8: bytes = b''

    def __init__(self, sampling_frequency: int, channel_count: int, num_seconds_per_audio_chunk: float,
                 frequency_steps: dict, barcode_filename: str, barcode_zone_timing: str):
        if sampling_frequency <= 0:
            raise ValueError('Invalid sampling frequency')
        else:
            self.sampling_frequency = sampling_frequency

        if channel_count != 1 and channel_count != 2:
            raise ValueError('Invalid channel count; pick mono or stereo')
        else:
            self.channel_count = channel_count

        if num_seconds_per_audio_chunk <= 0:
            raise ValueError('Must have more than 0 seconds per chunk of audio parsed')
        else:
            self.num_seconds_per_audio_chunk = num_seconds_per_audio_chunk

        if len(frequency_steps) != 16:
            raise ValueError('This program assumes valid conversion from a hex to a frequency')
        else:
            self.frequency_steps = frequency_steps

        if not str(barcode_filename).endswith('.wav'):
            raise ValueError('Invalid filename extension')
        else:
            self.barcode_filename = barcode_filename

        self.barcode_zone_timing = barcode_zone_timing

    def parse_message(self, message: str):
        self.message_utf8 = message.encode('utf-8')
        return self

    def build(self):
        message_utf8_with_ecc = RSCodec().encode(self.message_utf8)
        message_utf8_with_ecc_and_timing = self.barcode_zone_timing.encode('utf-8') + message_utf8_with_ecc + self.barcode_zone_timing.encode('utf-8')
        # print("message_utf8: " + str(self.message_utf8))
        # print("message_utf8_with_ecc: " + str(message_utf8_with_ecc))
        for ch in message_utf8_with_ecc_and_timing:
            lower_hex = ch % 16
            higher_hex = int(ch / 16)
            # print("Writing higher hex: " + str(higher_hex))
            tone = ToneBuilder.generate_tone(
                self.num_seconds_per_audio_chunk,
                self.frequency_steps[higher_hex],
                32767 / 2,
                self.sampling_frequency,
                self.channel_count)
            self.barcode_data.extend(tone)

            # print("Writing lower hex: " + str(lower_hex))
            tone = ToneBuilder.generate_tone(
                self.num_seconds_per_audio_chunk,
                self.frequency_steps[lower_hex],
                32767 / 2,
                self.sampling_frequency,
                self.channel_count)
            self.barcode_data.extend(tone)

        # sound_1 = ToneBuilder.generate_tone(
        #     2 * self.num_seconds_per_audio_chunk,  # duration
        #     1000,  # sound frequency
        #     32767 / 2,  # max amplitude
        #     self.sampling_frequency,
        #     self.channel_count
        # )
        #
        # sound_2 = ToneBuilder.generate_tone(
        #     1 * self.num_seconds_per_audio_chunk,  # duration
        #     500,  # sound frequency
        #     32767 / 2,  # max amplitude
        #     self.sampling_frequency,
        #     self.channel_count
        # )
        #
        # sound_3 = ToneBuilder.generate_tone(
        #     1 * self.num_seconds_per_audio_chunk,  # duration
        #     2000,  # sound frequency
        #     32767 / 2,  # max amplitude
        #     self.sampling_frequency,
        #     self.channel_count
        # )
        #
        # self.barcode_data.extend(sound_1)
        # self.barcode_data.extend(sound_2)
        # self.barcode_data.extend(sound_3)

        return self

    def save(self):
        sound_file = wave.open(self.barcode_filename, 'w')
        sound_file.setframerate(self.sampling_frequency)
        sound_file.setnchannels(self.channel_count)
        sound_file.setsampwidth(2)

        sound_file.writeframesraw(self.barcode_data)

        sound_file.close()

        return self
