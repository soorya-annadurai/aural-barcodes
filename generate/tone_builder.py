import numpy
import pygame


class ToneBuilder:
    @staticmethod
    def generate_tone(sound_duration: float, sound_frequency: float, maximum_amplitude: int, sampling_frequency: int,
                      channel_count: int):
        pygame.mixer.init(frequency=sampling_frequency, channels=channel_count)

        # calculate the number of sound cycles in the length of this sound
        cycles_count = int(sound_duration * sound_frequency)
        # calculate the number of samples per cycle
        samples_per_cycle = int((sampling_frequency * sound_duration) / cycles_count)

        # create a single vibration (range with step size = number of samples per cycle)
        sine = numpy.arange(0, 2 * numpy.pi, (2 * numpy.pi) / samples_per_cycle)
        # this is now just increasing numbers between 0 and 2 pi.
        # Apply a sinusoid to turn them into a wave function.
        sine = numpy.sin(sine)
        # multiply the current numbers by the maximum amplitude to make and audible sound
        sine *= maximum_amplitude

        # repeat the single cycle as much as we need to fill the current sound
        sine = numpy.hstack(int(cycles_count) * list(sine))

        # for stereo, double the samples, and reshape the single array to two arrays
        if channel_count == 2:
            sine = numpy.repeat(sine, 2, axis=0)
            sine.reshape(int(len(sine) / 2), 2)

        sound = pygame.mixer.Sound(sine.astype('int16'))

        return sound
