from generate.aural_barcode_builder import AuralBarcodeBuilder
from listen.aural_barcode_listener import AuralBarcodeListener

MESSAGE_TO_TRANSMIT = 'Auracodes are awesome.'

SAMPLING_FREQUENCY: int = 48000
CHANNEL_COUNT: int = 1
AURAL_BARCODE_FILENAME: str = 'aural_barcode.wav'
AURAL_BARCODE_ZONE_TIMING_CODE: str = '0909'
NUM_SECONDS_PER_AUDIO_CHUNK: float = 0.1
FREQUENCY_STEP_INTERVAL = 200
FREQUENCY_STEP_OFFSET = 600
FREQUENCY_STEPS = {
    0x0: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 0,  # 0000
    0x1: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 1,  # 0001
    0x2: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 2,  # 0010
    0x3: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 3,  # 0011
    0x4: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 4,  # 0100
    0x5: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 5,  # 0101
    0x6: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 6,  # 0110
    0x7: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 7,  # 0111
    0x8: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 8,  # 1000
    0x9: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 9,  # 1001
    0xA: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 10,  # 1010
    0xB: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 11,  # 1011
    0xC: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 12,  # 1100
    0xD: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 13,  # 1101
    0xE: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 14,  # 1110
    0xF: FREQUENCY_STEP_OFFSET + FREQUENCY_STEP_INTERVAL * 15,  # 1111
}

# FUTURE: Encrypt the message
# FUTURE: Use chords instead of individual frequencies to transfer more at the same time.
# FUTURE: Detect the timing zones from the audio stream, instead of presuming their existence

if __name__ == '__main__':
    barcode_builder = AuralBarcodeBuilder(SAMPLING_FREQUENCY, CHANNEL_COUNT, NUM_SECONDS_PER_AUDIO_CHUNK,
                                          FREQUENCY_STEPS, AURAL_BARCODE_FILENAME, AURAL_BARCODE_ZONE_TIMING_CODE)
    barcode_builder.parse_message(MESSAGE_TO_TRANSMIT).build().save()

    barcode_listener = AuralBarcodeListener(AURAL_BARCODE_FILENAME, NUM_SECONDS_PER_AUDIO_CHUNK, FREQUENCY_STEPS,
                                            AURAL_BARCODE_ZONE_TIMING_CODE)
    barcode_listener.parse().decode_message()
